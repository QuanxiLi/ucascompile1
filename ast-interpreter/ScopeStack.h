#include "Scope.h"
#include <deque>

class ScopeStack {
private:
    std::deque<Scope *> scopes; // scope inside is not owned by me
    NameScope global_scope;     // for global vars

public:
    ScopeStack(Stack *stack)
        : global_scope(stack) {}

    void pushScope(Scope *scope) {
        scopes.push_back(scope);
    }

    void popScope() {
        scopes.pop_back();
    }

    inline void *addVar(int bit_width) {
        return scopes.back()->addVar(bit_width);
    }

    inline void *addVar(QualType type, const ASTContext *context) {
        return scopes.back()->addVar(type, context);
    }

    inline void *declareVar(const VarDecl *decl, const ASTContext *context) {
        return scopes.back()->declareVar(decl, context);
    }

    inline void *declareGlobalVar(const VarDecl *decl, const ASTContext *context) {
        return global_scope.declareVar(decl, context);
    }

    void *findVar(const VarDecl *decl) const {
        void *global = global_scope.findVar(decl);
        if (global) {
            return global;
        }
        for (auto it = scopes.crbegin(); it != scopes.crend(); it++) {
            void *ptr = (*it)->findVar(decl);
            if (ptr != nullptr)
                return ptr;
        }
        throw std::runtime_error(std::string("variable `") + decl->getNameAsString() + "` not defined");
    }

    void *getRet() const {
        for (auto it = scopes.crbegin(); it != scopes.crend(); it++) {
            void *ptr = (*it)->getRet();
            if (ptr != nullptr) {
                return ptr;
            }
        }
        throw std::runtime_error("return outside a function");
    }
};