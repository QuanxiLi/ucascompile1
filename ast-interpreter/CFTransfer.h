class CFTransfer {
public:
    virtual ~CFTransfer() {}
};

// class CFGoto : public CFTransfer { /* ... */ }

#ifdef CFT_IMPL
CFTransfer CF_RETURN;
CFTransfer CF_BREAK;
CFTransfer CF_CONTINUE;
#endif

#define FORWARD_CFT(VISIT_STMT)                                      \
    if (CFTransfer *__cft = static_cast<CFTransfer *>(VISIT_STMT)) { \
        return __cft;                                                \
    }

#define HANDLE_CFT_IN_LOOP(VISIT_STMT)                         \
    CFTransfer *__cft = static_cast<CFTransfer *>(VISIT_STMT); \
    if (__cft == &CF_BREAK) {                                  \
        break;                                                 \
    }                                                          \
    if (__cft != nullptr && __cft != &CF_CONTINUE) {           \
        return __cft;                                          \
    }
