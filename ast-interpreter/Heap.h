#include <cstdlib>

class Heap {
public:
    static void *Alloc(size_t size) {
        return std::malloc(size);
    }

    static void Dealloc(void *ptr) {
        std::free(ptr);
    }
};