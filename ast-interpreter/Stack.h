#include <cstddef>
#include <cstdlib>
#include <iostream>
#include <stdexcept>

#define SAFE_STACK_ALLOCATION
constexpr size_t STACK_SIZE = 1024 * 1024;

class Stack {
public:
    typedef int StackElementType;
    typedef StackElementType *TopPtr;

private:
    TopPtr data;
    TopPtr top;
#ifdef SAFE_STACK_ALLOCATION
    TopPtr stack_end;
#endif
public:
    Stack() {
        data = new StackElementType[STACK_SIZE];
        top = data;
#ifdef SAFE_STACK_ALLOCATION
        stack_end = data + STACK_SIZE;
#endif
    }

    ~Stack() {
        delete[] data;
    }

    void *alloc(size_t size) {
        void *ptr = top;
        top += size / sizeof(StackElementType) + (size % sizeof(StackElementType) ? 1 : 0);
#ifdef SAFE_STACK_ALLOCATION
        if (top > stack_end) {
            throw std::runtime_error("stack overflow");
        }
#endif
        return static_cast<void *>(ptr);
    }

    template <typename T>
    T *typedAlloc() {
        return static_cast<T *>(alloc(sizeof(T)));
    }

    template <typename T>
    T *typedAlloc(size_t array_size) {
        return static_cast<T *>(alloc(sizeof(T) * array_size));
    }

    // --------------------------------
    // for class Scope
    TopPtr getTop() const {
        return top;
    }

    void setTop(TopPtr top) {
        this->top = top;
    }
    // --------------------------------

    // for debugging
    void print() const {
        std::cerr << "stack:\t";
        for (TopPtr p = data; p < top; p++) {
            std::cerr << *p << '\t';
        }
        std::cerr << std::endl;
    }
};