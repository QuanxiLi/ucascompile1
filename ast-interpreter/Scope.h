#include "Stack.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Decl.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include <map>
#include <stdexcept>

using namespace clang;

class Scope {
private:
    Stack::TopPtr stack_base;

protected:
    Stack *stack; // not owned

    explicit Scope(Stack *stack) : stack(stack) {
        stack_base = stack->getTop();
    }

    ~Scope() {
        stack->setTop(stack_base);
    }

public:
    inline void *addVar(int bit_width) {
        return stack->alloc(bit_width / 8);
    }

    inline void *addVar(TypeInfo ti) {
        return stack->alloc(ti.Width / 8);
    }
    inline void *addVar(QualType type, const ASTContext *context) {
        return addVar(context->getTypeInfo(type));
    }
    virtual void *declareVar(const VarDecl *decl, const ASTContext *context) = 0;

    virtual void *findVar(const VarDecl *decl) const {
        return nullptr;
    }
    virtual void *getRet() const {
        return nullptr;
    }
};

class NameScope : public Scope {
protected:
    std::map<const VarDecl *, void *> variables; // variables are stored on stack
public:
    explicit NameScope(Stack *stack) : Scope(stack) {
    }

    // not initialized by me
    void *declareVar(const VarDecl *decl, const ASTContext *context) override {
        if (variables.find(decl) != variables.end()) {
            throw std::runtime_error(std::string("variable `") + decl->getNameAsString() + "` multi-defined");
        }
        QualType type = decl->getType();
        void *ptr = addVar(type, context);
        variables[decl] = ptr;
        return ptr;
    }

    void *findVar(const VarDecl *decl) const override {
        auto it = variables.find(decl);
        if (it == variables.end()) {
            return nullptr;
        }
        return it->second;
    }
};

// for single expression
class TempScope : public Scope {
public:
    explicit TempScope(Stack *stack) : Scope(stack) {
    }

    virtual void *declareVar(const VarDecl *decl, const ASTContext *context) {
        throw std::runtime_error("Can not declare var in expressions");
    }
};

class FunctionScope : public NameScope {
private:
    const FunctionDecl *fdecl;
    void *ret = nullptr;

public:
    explicit FunctionScope(Stack *stack, const FunctionDecl *fdecl) : NameScope(stack), fdecl(fdecl) {}

    void *findVar(const VarDecl *decl) const override {
        auto it = variables.find(decl);
        if (it == variables.end()) {
            throw std::runtime_error(std::string("variable `") + decl->getNameAsString() + "` not defined");
        }
        return it->second;
    }

    inline void setRet(void *ptr) {
        ret = ptr;
    }

    void *getRet() const override {
        return ret;
    }

    void *declareArg(size_t n, const ASTContext *context) {
        const VarDecl *decl = fdecl->parameters()[n];
        QualType type = decl->getType();
        void *ptr = addVar(type, context);
        variables[decl] = ptr;
        return ptr;
    }
};