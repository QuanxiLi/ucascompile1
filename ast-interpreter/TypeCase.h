#define TYPE_CASE(KIND, TYPE, STMT) \
    case BuiltinType::Kind::KIND: { \
        typedef TYPE T;             \
        STMT;                       \
        break;                      \
    }

#define TYPE_CASES(TYPE_KIND, STMT)                    \
    switch (TYPE_KIND) {                               \
        TYPE_CASE(Int, int, STMT)                      \
        TYPE_CASE(Bool, bool, STMT)                    \
        TYPE_CASE(Char_U, char, STMT)                  \
        TYPE_CASE(Char_S, char, STMT)                  \
        TYPE_CASE(UChar, unsigned char, STMT)          \
        TYPE_CASE(SChar, signed char, STMT)            \
        TYPE_CASE(Short, short, STMT)                  \
        TYPE_CASE(UShort, unsigned short, STMT)        \
        TYPE_CASE(UInt, unsigned int, STMT)            \
        TYPE_CASE(Long, long, STMT)                    \
        TYPE_CASE(LongLong, long long, STMT)           \
        TYPE_CASE(ULong, unsigned long, STMT)          \
        TYPE_CASE(ULongLong, unsigned long long, STMT) \
    default:                                           \
        throw std::runtime_error("unknown type");      \
    }
