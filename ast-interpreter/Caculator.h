#include "clang/AST/Expr.h"
#include <stdexcept>

namespace Caculator {

template <typename T>
void nonAssignBinaryOp(BinaryOperator::Opcode op, void *result, T *lhs, T *rhs) {
    switch (op) {
#define NONASSIGN_OPERATION
#define BOOL_BINARY_OPERATION(Name, Op)                \
    case BO_##Name:                                    \
        *static_cast<bool *>(result) = (*lhs)Op(*rhs); \
        return;
#define BINARY_OPERATION(Name, Op)                  \
    case BO_##Name:                                 \
        *static_cast<T *>(result) = (*lhs)Op(*rhs); \
        return;

#include "Operations.def"
#undef NONASSIGN_OPERATION
    default:
        llvm::errs() << "non-assign op: " << (int)op << "\n";
        throw std::runtime_error("unknown binary operator");
    }
}

template <typename T>
T *assignBinaryOp(BinaryOperator::Opcode op, T *lhs, T *rhs) {
    switch (op) {
#define ASSIGN_OPERATION
#define BINARY_OPERATION(Name, Op) \
    case BO_##Name:                \
        return &((*lhs)Op(*rhs));
#include "Operations.def"
#undef ASSIGN_OPERATION
    default:
        llvm::errs() << "assign op: " << (int)op << "\n";
        throw std::runtime_error("unknown binary operator");
    }
}

// !NOTICE: `*` and `&` operations are not implemented HERE!!!
template <typename T>
void unaryOp(UnaryOperator::Opcode op, void *result, T *input) {
    // all of them are rvalue expressions
    switch (op) {
#define NONASSIGN_OPERATION
#define PRE_UNARY_OPERATION(Name, Op)           \
    case UO_##Name:                             \
        *static_cast<T *>(result) = Op(*input); \
        return;
#define POST_UNARY_OPERATION(Name, Op)          \
    case UO_##Name:                             \
        *static_cast<T *>(result) = (*input)Op; \
        return;
#include "Operations.def"
#undef NONASSIGN_OPERATION
    case UO_AddrOf:
        *static_cast<T **>(result) = input;
        return;
    default:
        llvm::errs() << "Op: " << (int)op << "\n";
        throw std::runtime_error("unknown unary operator");
    }
}

template <>
void unaryOp<bool>(UnaryOperator::Opcode op, void *result, bool *input) {
    if (op == UO_LNot) {
        *static_cast<bool *>(result) = !(*input);
    } else {
        throw std::runtime_error("unknown unary operator for boolean");
    }
}

}; // namespace Caculator