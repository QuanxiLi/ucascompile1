//==--- tools/clang-check/ClangInterpreter.cpp - Clang Interpreter tool --------------===//
//===----------------------------------------------------------------------===//

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/EvaluatedExprVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include <cstring>

using namespace clang;

#include "Caculator.h"
#include "Environment.h"
#include "TypeCase.h"
#define CFT_IMPL
#include "CFTransfer.h"
#include "Heap.h"
template <typename RetTy>
RetTy getIntValue(void *ptr, int bit_width) {
    switch (bit_width) {
    case 8:
        return *static_cast<int8_t *>(ptr);
        break;
    case 16:
        return *static_cast<int16_t *>(ptr);
        break;
    case 32:
        return *static_cast<int32_t *>(ptr);
        break;
    case 64:
        return *static_cast<int64_t *>(ptr);
        break;
    case 128:
        return *static_cast<__int128_t *>(ptr);
        break;
    default:
        throw std::runtime_error("unknown integer width");
    };
}

template <typename RetTy>
RetTy getUIntValue(void *ptr, int bit_width) {
    switch (bit_width) {
    case 8:
        return *static_cast<uint8_t *>(ptr);
        break;
    case 16:
        return *static_cast<uint16_t *>(ptr);
        break;
    case 32:
        return *static_cast<uint32_t *>(ptr);
        break;
    case 64:
        return *static_cast<uint64_t *>(ptr);
        break;
    case 128:
        return *static_cast<__uint128_t *>(ptr);
        break;
    default:
        throw std::runtime_error("unknown integer width");
    };
}

template <typename IntTy>
void setIntValue(void *ptr, int bit_width, IntTy value) {
    switch (bit_width) {
    case 8:
        *static_cast<int8_t *>(ptr) = value;
        break;
    case 16:
        *static_cast<int16_t *>(ptr) = value;
        break;
    case 32:
        *static_cast<int32_t *>(ptr) = value;
        break;
    case 64:
        *static_cast<int64_t *>(ptr) = value;
        break;
    case 128:
        *static_cast<__int128_t *>(ptr) = value;
        break;
    default:
        throw std::runtime_error("unknown integer width");
    };
}

template <typename UIntTy>
void setUIntValue(void *ptr, int bit_width, UIntTy value) {
    switch (bit_width) {
    case 8:
        *static_cast<uint8_t *>(ptr) = value;
        break;
    case 16:
        *static_cast<uint16_t *>(ptr) = value;
        break;
    case 32:
        *static_cast<uint32_t *>(ptr) = value;
        break;
    case 64:
        *static_cast<uint64_t *>(ptr) = value;
        break;
    case 128:
        *static_cast<__uint128_t *>(ptr) = value;
        break;
    default:
        throw std::runtime_error("unknown integer width");
    };
}

class InterpreterVisitor final : public ConstStmtVisitor<InterpreterVisitor, void *> {
private:
    Environment *mEnv;
    const ASTContext &context;
    ScopeStack scopes;
    Stack stack;

public:
    explicit InterpreterVisitor(const ASTContext &context, Environment *env)
        : context(context), mEnv(env),
          scopes(&stack) {
    }

    virtual ~InterpreterVisitor() {
    }

    void *Visit(const Stmt *stmt) {
        if (const ValueStmt *vstmt = dyn_cast<ValueStmt>(stmt)) {
            return VisitValueStmt(vstmt);
        }
        return ConstStmtVisitor::Visit(stmt);
    }

    int VisitMain(const FunctionDecl *main_decl) {
        FunctionScope main_function_scope(&stack, main_decl);
        scopes.pushScope(&main_function_scope);
        int main_ret;
        main_function_scope.setRet(&main_ret);
        Visit(main_decl->getBody());
        scopes.popScope();
        return main_ret;
    }

    // ----------------------------------------------------------------
    // ======== Stmt  ========
    // always return nullptr
    void *VisitStmt(const Stmt *stmt) {
        // llvm::errs() << "VisitStmt\n";
        throw std::runtime_error("unknown stmt");
        return nullptr;
    }

    void *VisitNullStmt(const NullStmt *stmt) {
        return nullptr;
    }

    void *VisitCompoundStmt(const CompoundStmt *stmt) {
        // llvm::errs() << "VisitCompoundStmt\n";
        CFTransfer *cft = nullptr;
        NameScope scope(&stack);
        scopes.pushScope(&scope);
        for (auto *substmt : stmt->children()) {
            if (substmt) {
                if (cft = static_cast<CFTransfer *>(this->Visit(substmt)))
                    break;
            }
        }
        scopes.popScope();
        return cft;
    }

    void *VisitIfStmt(const IfStmt *stmt) {
        // llvm::errs() << "VisitIfStmt\n";
        bool cond = VisitCondition(stmt->getCond());
        if (cond) {
            // llvm::errs() << "\tTrue\n";
            FORWARD_CFT(Visit(stmt->getThen()));
        } else {
            // llvm::errs() << "\tFalse\n";
            const Stmt *else_stmt = stmt->getElse();
            if (else_stmt) {
                FORWARD_CFT(Visit(else_stmt));
            }
        }
        return nullptr;
    }

    void *VisitSwitchStmt(const SwitchStmt *stmt) {
        // llvm::errs() << "VisitSwitchStmt\n";
        // TODO
        throw;
        return nullptr;
    }

    void *VisitWhileStmt(const WhileStmt *stmt) {
        // llvm::errs() << "VisitWhileStmt\n";
        const Expr *cond = stmt->getCond();
        const Stmt *body = stmt->getBody();
        while (VisitCondition(cond)) {
            // llvm::errs() << "\tgo while iteration\n";
            HANDLE_CFT_IN_LOOP(Visit(body));
        }
        return nullptr;
    }

    void *VisitDoStmt(const DoStmt *stmt) {
        // llvm::errs() << "VisitDoStmt\n";
        auto body = stmt->getBody();
        auto cond = stmt->getCond();
        do {
            HANDLE_CFT_IN_LOOP(Visit(body));
        } while (VisitCondition(cond));
        return nullptr;
    }

    void *VisitForStmt(const ForStmt *stmt) {
        // llvm::errs() << "VisitForStmt\n";
        if (auto init_stmt = stmt->getInit()) {
            const Expr *init_expr = dyn_cast<Expr>(init_stmt);
            if (!init_expr) {
                throw std::runtime_error("the init part of for statement must be an expression.");
            }
            __VisitExprStmt(init_expr);
        }
        const Expr *cond = stmt->getCond();
        const Expr *inc = stmt->getInc();
        const Stmt *body = stmt->getBody();
        while (cond == nullptr || VisitCondition(cond)) {
            HANDLE_CFT_IN_LOOP(Visit(body));
            if (inc)
                __VisitExprStmt(inc);
        }
        return nullptr;
    }

    void *VisitGotoStmt(const GotoStmt *stmt) {
        // llvm::errs() << "VisitGotoStmt\n";
        throw std::runtime_error("goto statemet not supported");
        return nullptr;
    }

    void *VisitIndirectGotoStmt(const IndirectGotoStmt *stmt) {
        // llvm::errs() << "VisitIndirectGotoStmt\n";
        throw std::runtime_error("indirect goto statemet not supported");
        return nullptr;
    }

    void *VisitContinueStmt(const ContinueStmt *stmt) {
        return &CF_CONTINUE;
    }

    void *VisitBreakStmt(const BreakStmt *stmt) {
        return &CF_BREAK;
    }

    void *VisitReturnStmt(const ReturnStmt *stmt) {
        // llvm::errs() << "VisitReturnStmt\n";
        // Expr * 	getRetValue () 返回值是Expr*
        void *ret = scopes.getRet();
        TempScope scope(&stack);
        scopes.pushScope(&scope);
        const Expr *ret_expr = stmt->getRetValue();
        void *ptr = VisitExpr(ret_expr);
        int width = context.getTypeInfo(ret_expr->getType()).Width;
        std::memcpy(ret, ptr, width / 8);
        scopes.popScope();
        return &CF_RETURN;
    }

    void VisitGlobalVarDecl(const VarDecl *vardecl) {
        void *ptr = scopes.declareGlobalVar(vardecl, &context);
        if (vardecl->hasInit()) {
            TempScope scope(&stack);
            scopes.pushScope(&scope);
            void *v = VisitExpr(vardecl->getInit());
            memcpy(ptr, v, context.getTypeInfo(vardecl->getType()).Width / 8);
            scopes.popScope();
        }
    }

    void *VisitDeclStmt(const DeclStmt *stmt) {
        // llvm::errs() << "VisitDeclStmt\n";
        for (auto it = stmt->decl_begin(); it != stmt->decl_end(); ++it) {
            Decl *decl = *it;
            if (VarDecl *vardecl = dyn_cast<VarDecl>(decl)) {
                // llvm::errs() << "Found VarDecl: " << vardecl->getName()
                //             << " with type: " << vardecl->getType().getAsString() << "\n";
                void *ptr = scopes.declareVar(vardecl, &context);
                if (vardecl->hasInit()) {
                    TempScope scope(&stack);
                    scopes.pushScope(&scope);
                    void *v = VisitExpr(vardecl->getInit());
                    memcpy(ptr, v, context.getTypeInfo(vardecl->getType()).Width / 8);
                    scopes.popScope();
                }
            }
        }
        return nullptr;
    }

    void *VisitCaseStmt(const CaseStmt *stmt) {
        // llvm::errs() << "VisitCaseStmt\n";
        for (auto *substmt : stmt->children()) {
            if (substmt)
                FORWARD_CFT(this->Visit(substmt));
        }
        return nullptr;
    }

    void *VisitDefaultStmt(const DefaultStmt *stmt) {
        // llvm::errs() << "VisitDefaultStmt\n";
        for (auto *substmt : stmt->children()) {
            if (substmt)
                FORWARD_CFT(this->Visit(substmt));
        }
        return nullptr;
    }

    void *VisitCapturedStmt(const CapturedStmt *stmt) {
        // llvm::errs() << "VisitCapturedStmt\n";
        // TODO
        return nullptr;
    }

    // This is an abstract class but i would visit it
    // see Visit
    void *VisitValueStmt(const ValueStmt *stmt) {
        // llvm::errs() << "VisitValueStmt\n";
        // it may be Expr, LabelStmt or AttributedStmt
        // TODO: LabelStmt?
        if (const Expr *expr = dyn_cast<Expr>(stmt)) {
            __VisitExprStmt(expr);
        }
        return nullptr;
    }

    inline void __VisitExprStmt(const Expr *expr) {
        TempScope scope(&stack);
        scopes.pushScope(&scope);
        VisitExpr(expr);
        // stack.print();
        scopes.popScope();
    }

    bool VisitCondition(const Expr *cond) {
        auto cond_type = cond->getType();
        if (!(cond_type->isIntegerType() || cond_type->isPointerType())) {
            throw std::runtime_error(std::string("Type ") + cond_type.getAsString() + " can not be a condition.");
        }
        TempScope scope(&stack);
        scopes.pushScope(&scope);
        auto cond_ptr = VisitExpr(cond);
        bool bvalue;
        int bit_width = context.getTypeInfo(cond_type).Width;
        switch (bit_width) {
        case 8:
            bvalue = *static_cast<int8_t *>(cond_ptr);
            break;
        case 16:
            bvalue = *static_cast<int16_t *>(cond_ptr);
            break;
        case 32:
            bvalue = *static_cast<int32_t *>(cond_ptr);
            break;
        case 64:
            bvalue = *static_cast<int64_t *>(cond_ptr);
            break;
        case 128:
            bvalue = *static_cast<__int128_t *>(cond_ptr);
            break;
        default:
            throw std::runtime_error("unknown integer width");
        };
        scopes.popScope();
        return bvalue;
    }

    // ----------------------------------------------------------------
    // ======== Expr  ========
    // return pointer to the result

    inline void *VisitExpr(const Expr *expr) {
        return ConstStmtVisitor::Visit(expr);
    }

    void *VisitPredefinedExpr(const PredefinedExpr *expr) {
        // llvm::errs() << "VisitPredefinedExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitDeclRefExpr(const DeclRefExpr *expr) {
        // llvm::errs() << "VisitDeclRefExpr\n";
        auto decl = expr->getDecl();
        const VarDecl *vardecl = dyn_cast<VarDecl>(decl);
        if (!vardecl) {
            throw std::runtime_error("Only reference of variable supported now");
        }
        return scopes.findVar(vardecl);
    }

    void *VisitIntegerLiteral(const IntegerLiteral *expr) {
        // llvm::errs() << "VisitIntegerLiteral\n";
        QualType qtype = expr->getType();
        void *ptr = scopes.addVar(qtype, &context);
        auto apint_value = expr->getValue();
        int bit_width = context.getTypeInfo(qtype).Width;
        uint64_t limit = UINT64_MAX;
        if (apint_value.ugt(limit)) {
            throw std::runtime_error("integer literal too large");
        }
        uint64_t value = apint_value.getZExtValue();
        setIntValue(ptr, bit_width, value);
        return ptr;
    }

    void *VisitFixedPointLiteral(const FixedPointLiteral *expr) {
        // llvm::errs() << "VisitFixedPointLiteral\n";
        throw std::runtime_error("fixed point value not supported now");
        return nullptr;
    }

    void *VisitFloatingLiteral(const FloatingLiteral *expr) {
        // llvm::errs() << "VisitFloatingLiteral\n";
        throw std::runtime_error("float value not supported now");
        return nullptr;
    }

    void *VisitImaginaryLiteral(const ImaginaryLiteral *expr) {
        // llvm::errs() << "VisitImaginaryLiteral\n";
        throw std::runtime_error("complex value not supported now");
        return nullptr;
    }

    void *VisitStringLiteral(const StringLiteral *expr) {
        // llvm::errs() << "VisitStringLiteral\n";
        // TODO
        return nullptr;
    }

    void *VisitCharacterLiteral(const CharacterLiteral *expr) {
        // llvm::errs() << "VisitCharacterLiteral\n";
        QualType qtype = expr->getType();
        void *ptr = scopes.addVar(qtype, &context);
        unsigned value = expr->getValue();
        int bit_width = context.getTypeInfo(qtype).Width;
        setIntValue(ptr, bit_width, value);
        return ptr;
    }

    void *VisitParenExpr(const ParenExpr *expr) {
        // llvm::errs() << "VisitParenExpr\n";
        return VisitExpr(expr->getSubExpr());
    }

    void *VisitUnaryOperator(const UnaryOperator *expr) {
        // llvm::errs() << "VisitUnaryOperator\n";
        auto op = expr->getOpcode();
        auto subexpr = expr->getSubExpr();
        void *subexpr_valptr = VisitExpr(subexpr);
        if (op == UO_Deref) {
            return *static_cast<void **>(subexpr_valptr);
        }
        QualType qtype = expr->getType();
        const auto btype = dyn_cast<BuiltinType>(subexpr->getType().getTypePtr());
        if (btype == nullptr) {
            throw std::runtime_error("only builtin types supported for unary operator");
        }
        auto kind = btype->getKind();
        void *result = scopes.addVar(qtype, &context);
        TYPE_CASES(kind, Caculator::unaryOp<T>(op, result, static_cast<T *>(subexpr_valptr)));
        return result;
    }

    void *VisitOffsetOfExpr(const OffsetOfExpr *expr) {
        // llvm::errs() << "VisitOffsetOfExpr\n";
        throw std::runtime_error("OffsetOfExpr not supported now");
        return nullptr;
    }

    void *VisitUnaryExprOrTypeTraitExpr(const UnaryExprOrTypeTraitExpr *expr) {
        // llvm::errs() << "VisitUnaryExprOrTypeTraitExpr\n";
        void *ptr = scopes.addVar(sizeof(size_t));
        QualType type = expr->getTypeOfArgument();
        if (const ReferenceType *ref = type->getAs<ReferenceType>())
            type = ref->getPointeeType();
        switch (expr->getKind()) {
        case UETT_PreferredAlignOf:
        case UETT_AlignOf: {
            *static_cast<size_t *>(ptr) = context.getTypeInfo(type).Align / 8;
            break;
        }
        case UETT_SizeOf: {
            *static_cast<size_t *>(ptr) = context.getTypeInfo(type).Width / 8;
            break;
        }
        default:
            throw std::runtime_error("unknown UnaryExprOrTypeTraitExpr type");
        }
        return ptr;
    }

    void *VisitArraySubscriptExpr(const ArraySubscriptExpr *expr) {
        // llvm::errs() << "VisitArraySubscriptExpr\n";
        void *base = VisitExpr(expr->getBase());
        char *base_value = *static_cast<char **>(base);
        auto idx_expr = expr->getIdx();
        void *idx = VisitExpr(idx_expr);
        size_t idx_value = getIntValue<size_t>(idx, context.getTypeInfo(idx_expr->getType()).Width);
        int element_align = context.getTypeInfo(expr->getType()).Align;
        return base_value + idx_value * element_align / 8;
    }

    void *VisitCallExpr(const CallExpr *expr) {
        // llvm::errs() << "VisitCallExpr\n";
        const FunctionDecl *Callee = expr->getDirectCallee();
        if (Callee == mEnv->getOutputDecl()) {
            const Expr *decl = expr->getArg(0);
            void *ptr = VisitExpr(decl);
            int val = *static_cast<int *>(ptr);
            llvm::errs() << val << '\n';
            return nullptr;
        } else if (Callee == mEnv->getInputDecl()) {
            llvm::errs() << "Please Input an Integer Value :";
            int val;
            std::cin >> val;
            QualType ty = Callee->getReturnType();
            void *retptr = scopes.addVar(ty, &context);
            *static_cast<int *>(retptr) = val;
            return retptr;
        } else if (Callee == mEnv->getMallocDecl()) {
            const Expr *decl = expr->getArg(0);
            void *ptr = VisitExpr(decl);
            int val = *static_cast<int *>(ptr);
            void **ret_ptr = static_cast<void **>(scopes.addVar(sizeof(void *) * 8));
            *ret_ptr = Heap::Alloc(val);
            return ret_ptr;
        } else if (Callee == mEnv->getFreeDecl()) {
            const Expr *decl = expr->getArg(0);
            void *ptr = VisitExpr(decl);
            void *val = *static_cast<void **>(ptr);
            Heap::Dealloc(val);
            return nullptr;
        } else {
            QualType ty = Callee->getReturnType();
            int count = 0;
            void *retptr = scopes.addVar(ty, &context);
            FunctionScope scope(&stack, Callee);
            scope.setRet(retptr); // TODO: what happens if return void?
            for (auto it = expr->arg_begin(); it != expr->arg_end(); ++it) {
                const Expr *arg = *it;
                void *tmp = scope.declareArg(count++, &context);
                TempScope tmpscope(&stack);
                scopes.pushScope(&tmpscope);
                void *ptr = VisitExpr(arg);
                int width = context.getTypeInfo(arg->getType()).Width;
                std::memcpy(tmp, ptr, width / 8);
                scopes.popScope();
            }
            scopes.pushScope(&scope);
            Visit(Callee->getBody());
            void *revalue = scopes.getRet();
            scopes.popScope();
            return revalue;
        }
    }

    void *VisitMemberExpr(const MemberExpr *expr) {
        // llvm::errs() << "VisitMemberExpr\n";
        throw std::runtime_error("member expression not supported now");
        return nullptr;
    }

    void *VisitImplicitCastExpr(const ImplicitCastExpr *expr) {
        // llvm::errs() << "VisitImplicitCastExpr\n";
        return VisitCastExpr(expr);
    }

    void *VisitCStyleCastExpr(const CStyleCastExpr *expr) {
        // llvm::errs() << "VisitCStyleCastExpr\n";
        return VisitCastExpr(expr);
    }

    void *VisitCastExpr(const CastExpr *expr) {
        switch (expr->getCastKind()) {
        case CK_BitCast:
        case CK_LValueBitCast:
        case CK_LValueToRValueBitCast:
        case CK_LValueToRValue:
        case CK_NoOp:
            return VisitExpr(expr->getSubExpr());
        case CK_ArrayToPointerDecay: {
            void **ptr = static_cast<void **>(scopes.addVar(sizeof(void *) * 8));
            *ptr = VisitExpr(expr->getSubExpr());
            return ptr;
        }
        case CK_FunctionToPointerDecay:
            throw std::runtime_error("TODO: CK_FunctionToPointerDecay"); // TODO
        case CK_NullToPointer:
        case CK_IntegralToPointer:
        case CK_PointerToIntegral:
        case CK_IntegralCast: {
            auto subexpr = expr->getSubExpr();
            auto subvalue = VisitExpr(subexpr);
            auto src_type = subexpr->getType();
            auto dest_type = expr->getType();
            int src_width = context.getTypeInfo(src_type).Width;
            int dest_width = context.getTypeInfo(dest_type).Width;
            if (dest_width == src_width) {
                return subvalue;
            }
            void *ptr = scopes.addVar(dest_width);
            if (dest_type->isSignedIntegerType()) {
                intmax_t value = getIntValue<intmax_t>(subvalue, src_width);
                setIntValue(ptr, dest_width, value);
            } else {
                uintmax_t value = getUIntValue<uintmax_t>(subvalue, src_width);
                setUIntValue(ptr, dest_width, value);
            }
            return ptr;
        }
        default:
            throw std::runtime_error("unknown cast");
        }
    }

    void *__VisitBinaryOperatorForPtr(const BinaryOperator *expr, QualType ltype, QualType rtype) {
        void *lhs = VisitExpr(expr->getLHS());
        void *rhs = VisitExpr(expr->getRHS());
        auto op = expr->getOpcode();
        if (rtype->isIntegerType() || op == BO_Add) {
            // ptr op int
            if (op == BO_Add && ltype->isIntegerType()) {
                std::swap(lhs, rhs);
                std::swap(ltype, rtype);
            }
            QualType pointee_type = dyn_cast<PointerType>(ltype.getTypePtr())->getPointeeType();
            int rwidth = context.getTypeInfo(rtype).Width;
            int align = context.getTypeInfo(pointee_type).Align;
            if (rtype->isSignedIntegerType()) {
                intptr_t ival = getIntValue<intptr_t>(rhs, rwidth);
                ival *= align / 8;
                void *ret;
                switch (op) {
                case BO_Add:
                    ret = scopes.addVar(sizeof(void *) * 8);
                    *static_cast<char **>(ret) = *static_cast<char **>(lhs) + ival;
                    return ret;
                case BO_Sub:
                    ret = scopes.addVar(sizeof(void *) * 8);
                    *static_cast<char **>(ret) = *static_cast<char **>(lhs) - ival;
                    return ret;
                case BO_AddAssign:
                    return *static_cast<char **>(lhs) += ival;
                case BO_SubAssign:
                    return *static_cast<char **>(lhs) -= ival;
                }
                throw std::runtime_error("invalid operator between pointer and integer");
            } else {
                intptr_t uval = getIntValue<uintptr_t>(rhs, rwidth);
                uval /= align / 8;
                void *ret;
                switch (op) {
                case BO_Add:
                    ret = scopes.addVar(sizeof(void *) * 8);
                    *static_cast<char **>(ret) = *static_cast<char **>(lhs) + uval;
                    return ret;
                case BO_Sub:
                    ret = scopes.addVar(sizeof(void *) * 8);
                    *static_cast<char **>(ret) = *static_cast<char **>(lhs) - uval;
                    return ret;
                case BO_AddAssign:
                    return *static_cast<char **>(lhs) += uval;
                case BO_SubAssign:
                    return *static_cast<char **>(lhs) -= uval;
                }
                throw std::runtime_error("invalid operator between pointer and integer");
            }
        } else {
            // ptr op ptr
            if (BinaryOperator::isAssignmentOp(op)) {
                Caculator::assignBinaryOp<uintptr_t>(op, static_cast<uintptr_t *>(lhs), static_cast<uintptr_t *>(rhs));
                if (op == BO_Sub || op == BO_SubAssign) {
                    QualType pointee_type = dyn_cast<PointerType>(ltype.getTypePtr())->getPointeeType();
                    *static_cast<uintptr_t *>(lhs) /= context.getTypeInfo(pointee_type).Align / 8;
                }
                return lhs;
            } else {
                uintptr_t result_v;
                Caculator::nonAssignBinaryOp<uintptr_t>(op, &result_v, static_cast<uintptr_t *>(lhs), static_cast<uintptr_t *>(rhs));
                if (op == BO_Sub || op == BO_SubAssign) {
                    QualType pointee_type = dyn_cast<PointerType>(ltype.getTypePtr())->getPointeeType();
                    result_v /= context.getTypeInfo(pointee_type).Align / 8;
                }
                int result_width = context.getTypeInfo(expr->getType()).Width;
                void *result = scopes.addVar(result_width);
                if (expr->getType()->isSignedIntegerType()) {
                    setIntValue<intptr_t>(result, result_width, result_v);
                } else {
                    setUIntValue<uintptr_t>(result, result_width, result_v);
                }
                return result;
            }
        }
    }

    void *VisitBinaryOperator(const BinaryOperator *expr) {
        // llvm::errs() << "VisitBinaryOperator\n";
        QualType ltype = expr->getLHS()->getType();
        QualType rtype = expr->getRHS()->getType();
        if (ltype->isPointerType()) {
            return __VisitBinaryOperatorForPtr(expr, ltype, rtype);
        } else if (rtype->isPointerType()) {
            throw;
        }
        const auto btype = dyn_cast<BuiltinType>(ltype.getTypePtr());
        if (btype == nullptr) {
            throw std::runtime_error("only builtin types supported for binary operator");
        }
        auto kind = btype->getKind();
        auto op = expr->getOpcode();
        void *lhs = VisitExpr(expr->getLHS());
        void *rhs = VisitExpr(expr->getRHS());
        if (BinaryOperator::isAssignmentOp(op)) {
            TYPE_CASES(kind, return Caculator::assignBinaryOp<T>(op, static_cast<T *>(lhs), static_cast<T *>(rhs)));
        } else {
            void *result = scopes.addVar(ltype, &context);
            TYPE_CASES(kind, Caculator::nonAssignBinaryOp<T>(op, result, static_cast<T *>(lhs), static_cast<T *>(rhs)));
            return result;
        }
    }

    void *VisitConditionalOperator(const ConditionalOperator *expr) {
        // llvm::errs() << "VisitConditionalOperator\n";
        // TODO
        return nullptr;
    }

    void *VisitCompoundLiteralExpr(const CompoundLiteralExpr *expr) {
        // llvm::errs() << "VisitCompoundLiteralExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitInitListExpr(const InitListExpr *expr) {
        // llvm::errs() << "VisitInitListExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitDesignatedInitExpr(const DesignatedInitExpr *expr) {
        // llvm::errs() << "VisitDesignatedInitExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitDesignatedInitUpdateExpr(const DesignatedInitUpdateExpr *expr) {
        // llvm::errs() << "VisitDesignatedInitUpdateExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitImplicitValueInitExpr(const ImplicitValueInitExpr *expr) {
        // llvm::errs() << "VisitImplicitValueInitExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitNoInitExpr(const NoInitExpr *expr) {
        // llvm::errs() << "VisitNoInitExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitArrayInitLoopExpr(const ArrayInitLoopExpr *expr) {
        // llvm::errs() << "VisitArrayInitLoopExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitArrayInitIndexExpr(const ArrayInitIndexExpr *expr) {
        // llvm::errs() << "VisitArrayInitIndexExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitParenListExpr(const ParenListExpr *expr) {
        // llvm::errs() << "VisitParenListExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitVAArgExpr(const VAArgExpr *expr) {
        // llvm::errs() << "VisitVAArgExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitGenericSelectionExpr(const GenericSelectionExpr *expr) {
        // llvm::errs() << "VisitGenericSelectionExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitPseudoObjectExpr(const PseudoObjectExpr *expr) {
        // llvm::errs() << "VisitPseudoObjectExpr\n";
        // TODO
        return nullptr;
    }

    void *VisitSourceLocExpr(const SourceLocExpr *expr) {
        // llvm::errs() << "VisitSourceLocExpr\n";
        // TODO
        return nullptr;
    }
};

void Environment::init(TranslationUnitDecl *unit, InterpreterVisitor &mVisitor) {
    context = &(unit->getASTContext());
    for (TranslationUnitDecl::decl_iterator i = unit->decls_begin(), e = unit->decls_end(); i != e; ++i) {
        if (FunctionDecl *fdecl = dyn_cast<FunctionDecl>(*i)) {
            if (fdecl->getName().equals("FREE"))
                mFree = fdecl;
            else if (fdecl->getName().equals("MALLOC"))
                mMalloc = fdecl;
            else if (fdecl->getName().equals("GET"))
                mInput = fdecl;
            else if (fdecl->getName().equals("PRINT"))
                mOutput = fdecl;
            else if (fdecl->getName().equals("main"))
                mEntry = fdecl;
        } else if (VarDecl *vardecl = dyn_cast<VarDecl>(*i)) {
            mVisitor.VisitGlobalVarDecl(vardecl);
        }
    }
}

class InterpreterConsumer : public ASTConsumer {
public:
    explicit InterpreterConsumer(const ASTContext &context)
        : mEnv(), mVisitor(context, &mEnv) {
    }
    virtual ~InterpreterConsumer() {}

    virtual void HandleTranslationUnit(clang::ASTContext &Context) {
        TranslationUnitDecl *decl = Context.getTranslationUnitDecl();
        mEnv.init(decl, mVisitor);

        FunctionDecl *entry = mEnv.getEntry();
        int main_ret = mVisitor.VisitMain(entry);
        // llvm::errs() << "program finished with code " << main_ret << "\n";
    }

private:
    Environment mEnv;
    InterpreterVisitor mVisitor;
};

class InterpreterClassAction : public ASTFrontendAction {
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
        clang::CompilerInstance &Compiler, llvm::StringRef InFile) {
        return std::unique_ptr<clang::ASTConsumer>(
            new InterpreterConsumer(Compiler.getASTContext()));
    }
};

int main(int argc, char **argv) {
    if (argc > 1) {
        clang::tooling::runToolOnCode(std::unique_ptr<clang::FrontendAction>(new InterpreterClassAction), argv[1]);
    }
}
