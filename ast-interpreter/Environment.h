//==--- tools/clang-check/ClangInterpreter.cpp - Clang Interpreter tool --------------===//
//===----------------------------------------------------------------------===//
#include <stdio.h>

#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Decl.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"

#include "ScopeStack.h"

using namespace clang;

class InterpreterVisitor;

class Environment {
    ASTContext *context = nullptr;

    FunctionDecl *mFree = nullptr; /// Declartions to the built-in functions
    FunctionDecl *mMalloc = nullptr;
    FunctionDecl *mInput = nullptr;
    FunctionDecl *mOutput = nullptr;

    FunctionDecl *mEntry = nullptr;

public:
    /// Get the declartions to the built-in functions
    explicit Environment() {
    }

    const FunctionDecl *getInputDecl() const {
        return mInput;
    }
    const FunctionDecl *getOutputDecl() const {
        return mOutput;
    }
    const FunctionDecl *getMallocDecl() const {
        return mMalloc;
    }
    const FunctionDecl *getFreeDecl() const {
        return mFree;
    }

    /// Initialize the Environment
    void init(TranslationUnitDecl *unit, InterpreterVisitor &mVisitor);

    FunctionDecl *getEntry() {
        return mEntry;
    }
};
